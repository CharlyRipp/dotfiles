# DOTFILES

Some useful bash shortcuts, commands, aliases and more.

## Pre-reqs

##### ZSH

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

##### ZSH Plugins

```bash
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-completions
```

## Installation
Clone this repo to `~/.dotfiles` directory and source the `~/.dotfiles/index.sh` at the bottom of `~/.zshrc`.

```bash
git clone https://gitlab.com/CharlyRipp/dotfiles.git ~/.dotfiles
echo "source ~/.dotfiles/index.sh" >> ~/.zshrc
```

Also recommended to edit `.zshrc` and enable plugins:
```bash
plugins=(
  osx
  git
  zsh-autosuggestions
  zsh-syntax-highlighting
  zsh-completions
  history-substring-search
  docker
  brew
)
autoload -U compinit && compinit
```

## Update

The `df-update` can be executed, which executes as `git pull` on this repo and all installed plugins.