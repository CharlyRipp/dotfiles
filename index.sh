#!/usr/bin/env bash

export DF_DIR=`dirname $0`
export DF_USER=$(echo $USER | tr "[:upper:]" "[:lower:]")

setopt +o nomatch
source-all() {
  for DOTFILE in `find "$1"/*.sh`; do
    [ -f $DOTFILE ] && source $DOTFILE
  done
}

mkdir -p ${DF_DIR}/custom
test -f ${DF_DIR}/custom/config.sh || cat <<EOF > ${DF_DIR}/custom/config.sh
#!/usr/bin/env bash
# This is the very first sourced file, set environment variables, configure plugins, etc. here
# Note: Set environment variables in index.sh if attempting to override a plugin's settings

EOF

test -f ${DF_DIR}/custom/index.sh || cat <<EOF > ${DF_DIR}/custom/index.sh
#!/usr/bin/env bash

# This is sourced last. Put anything custom here with the ability to use all dotfiles and plugins
# For example these may have been set by a plugin, overriding here will override the plugin's setting
# export DF_JUMP_SERVER=
# export DF_PROXY_ADDR=
# export DF_NETWORK_PROXY_ON=
# export DF_CD_LL=1

EOF


source $DF_DIR/custom/config.sh

# Add bins to path
export PATH="/usr/local/sbin:${HOME}/bin:$PATH"

# Remove end of line marker in zsh
export PROMPT_EOL_MARK=""

# Easy update function
df-update() {
  local dir=$PWD
  cds ${DF_DIR} && git-update
  cds ${dir}
}

# Performs a `ll -a` after chaning directory
chpwd() {
  emulate -L zsh
  [[ "$DF_CD_LL" ]] && ll -a
}

# Silent cd incase ll is turned on
cds() {
  cd $1 > /dev/null
}

# Make directory and cd to it (dir)
mk() {
  mkdir -p "$1" && cd "$1"
}

# Easy delete dir/files (dir)
del() {
  rm -fr "$1"
}

enable-word-split() {
  [ shell-is-zsh ] && setopt shwordsplit
}

disable-word-split() {
  [ shell-is-zsh ] && unsetopt shwordsplit
}

shell-is-zsh() {
  local shell=$(ps -p $$ -o comm="")
  [ "$shell" = "zsh" ] && echo true || echo false
}

# Clipboard alias
alias cb="pbcopy"

alias brewup='cds ~; brew update; brew upgrade; brew upgrade --casks; brew cleanup; brew doctor; cds -'

alias t=todolist

# Change to directory last opened by finder
cdf() {
    target=`osascript -e 'tell application "Finder" to if (count of Finder windows) > 0 then get POSIX path of (target of front Finder window as text)'`
    if [ "$target" != "" ]; then
        cd "$target"
    else
        echo 'No Finder window found' >&2
    fi
}

catc() {
    local out colored
    out=$(/bin/cat $@)
    colored=$(echo $out | pygmentize -f console -g 2>/dev/null)
    [[ -n $colored ]] && echo "$colored" || echo "$out"
}

jump() {
  ssh -J $DF_USER@$DF_JUMP_SERVER $DF_USER@$1
}

# Send file to jump server (local file, remote file)
jump-upload() {
  scp "$1" $DF_USER@${3:-$DF_JUMP_SERVER}:~/$2
}

jump-send() {
  scp -oProxyJump="$DF_USER@${4:-$DF_JUMP_SERVER}" "$DF_USER@$2" $1:~/${3:-$2}
}

# Copy public key to jump server to skip password prompts on future jumps
jump-copy-id() {
  ssh-copy-id -i ~/.ssh/id_rsa.pub $DF_USER@${1:-$DF_JUMP_SERVER}
}

# Generate a new ssh key
ssh-gen() {
    DIR=${2:-~/.ssh/id_rsa}
    ssh-keygen -t rsa -C "$1" -b 4096 -N "" -f $DIR
}

# Set all [array] to `value`
# Example: set_all arr "$value"
set-all() {
    name=$1[@]
    value=$2
    vars=("${!name}")

    for i in "${vars[@]}" ; do
      export $i="${value}"
    done
}

urlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"
}

# to get user password, used in other .dotfiles
get-password() {
  echo "$(security find-internet-password -a ${USER} -g -w)"
}

whats-on-port() {
  echo `sudo lsof -i :$1`
}

whats-my-internal-ip() {
  echo `ipconfig getifaddr en0`
}

whats-my-external-ip() {
  echo `curl -s ipinfo.io/ip`
}

# Switch network locations
network() {
  sudo networksetup -switchtolocation "$1"
  sudo networksetup -setairportnetwork en0 $2
}

bounce-network() {
    networksetup -setairportpower en0 off
    sleep 5
    networksetup -setairportpower en0 on
}

# Retrieve the current active network service
get-current-network-device() {
  devices=$(networksetup -listnetworkserviceorder | grep 'Hardware Port')

  while read line; do
      sname=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $2}')
      sdev=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $4}')
      if [ -n "$sdev" ]; then
          ifconfig $sdev 2>/dev/null | grep 'status: active' > /dev/null 2>&1
          rc="$?"
          if [ "$rc" -eq 0 ]; then
              currentdevice="$sname"
          fi
      fi
  done <<< "$(echo "$devices")"

  if [ -n $currentdevice ]; then
      echo $currentdevice
  else
      >&2 echo "Could not find current device"
      exit 1
  fi
}

get-no-proxy-list() {
  networksetup -getproxybypassdomains "`get-current-network-device`" |
  while read interface; do
    local no_proxies="$no_proxies, $interface"
  done
  echo $no_proxies | cut -c 3-
}

get-proxy() {
  echo "http://${USER}:$(urlencode `get-password`)@${DF_PROXY_ADDR}"
}

update-proxy() {
  if [[ "$(networksetup -getcurrentlocation)" = $DF_NETWORK_PROXY_ON ]]; then
    proxy-on $1
  else
    proxy-off $1
  fi
}

# Proxy managment functions
proxy-on() {
  export http_proxy="`get-proxy`"
  export https_proxy="${http_proxy}"
  export no_proxy="`get-no-proxy-list`"
  export HTTP_PROXY="${http_proxy}"
  export HTTPS_PROXY="${https_proxy}"
  export NO_PROXY="${no_proxy}"
  proxy-status $1
}

proxy-off() {
  unset http_proxy https_proxy no_proxy HTTP_PROXY HTTPS_PROXY NO_PROXY
  proxy-status $1
}

proxy-status() {
  if [ -n "$1" ] && [ $1 = "-q" ]; then return; fi
  
  if [ -z ${http_proxy+x} ]; then
    local proxy="Disabled"
  else
    local proxy="Enabled"
  fi
  local profile=`networksetup -getcurrentlocation`
  local network=$(echo `networksetup -getairportnetwork en0` | awk '{print $4}')

  printf "Status:  $proxy\n"
  printf "Network: $network\n"
  printf "Profile: $profile\n"
  printf "Proxy:   $DF_PROXY_ADDR\n"
}

poll-for-200() {
    ENDPOINT=$1
    if [ $ENDPOINT = "" ]; then
        echo "No endpoint given!"
    fi

    WAIT=${2:-1}
    ATTEMPTS=${3:-10}
    
    for ((n=0;n<$ATTEMPTS;n++)) 
    do
        if [ $n -gt 0 ]; then sleep $WAIT; fi
        response=`curl --write-out '%{http_code}' --silent --output /dev/null $ENDPOINT`
        if [ $response -eq 200 ]; then return 0; fi
    done

    return 1
}

cert-get() {
  echo | openssl s_client -showcerts -servername $1 -connect $1 2>/dev/null | openssl x509 -inform pem -noout -text
}

source-all $DF_DIR/third-party
for DOTFILE in $DF_DIR/plugins/*/index.sh ; do
    [ -f $DOTFILE ] && source $DOTFILE
done

source $DF_DIR/custom/index.sh
setopt +1 nomatch
