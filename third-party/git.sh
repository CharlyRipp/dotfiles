#!/bin/bash

# Easy utilities for Git

git-url() {
  echo `git config --get remote.origin.url | sed "s/.*\@\(.*\):\(.*\)\..*/https:\/\/\1\/\2/"`
}

# Open current directory project in browser
git-ui() {
  branch=`git branch | cut -d' ' -f 2 | sed '/^$/d'`
  UI_URL=`git-url`
  open "${UI_URL}/tree/${branch}"
}

# git-clone {repo} [base dir]
git-clone() {
  REPO=$1
  BASE_DIR=${2:-~}
  HTTPS=false

  if [ -z `echo $REPO | sed -n "/^https:/p"` ]; then
    NS=`echo $REPO | sed "s/.*\@\(.*\):\(.*\)\/\(.*\)\..*/\1\/\2/"`
  else
    NS=`echo $REPO | sed "s/https:\/\/\(.*\)\/\(.*\)\..*/\1/"`
    HTTPS=true
  fi

  mkdir -p $BASE_DIR/$NS
  cd $BASE_DIR/$NS
  git clone $REPO
  
  if $HTTPS; then
    cd `echo $REPO | sed "s/https:\/\/\(.*\)\/\(.*\)\..*/\2/"`
  else
    cd `echo $REPO | sed "s/.*\@\(.*\)\..*:\(.*\)\/\(.*\)\..*/\3/"`
  fi
}

git-p() {
  MSG=${1:-"Quick Push"}
  git add .
  git commit -m $MSG 
  git push $([ -z "$2" ] && echo "" || echo "--push-option=ci.skip")
}

git-push() {
  if [[ -n "$(git status --porcelain)" ]]; then
    git stash
    git pull
    git stash pop

    CONFLICTS=$(git ls-files -u | wc -l)
    if [ "$CONFLICTS" -gt 0 ] ; then
      echo "Resolve conflicts!"
      return 1
    fi

    git-p $1
  else
    echo "No changes detected"
  fi
}

git-update() {
  for REPO in `find . -name '.git' -type d`
  do
    DIR=`echo $REPO | sed "s/\(.*\)\/\.git/\1/"`
    cds $DIR
    echo "\nUpdating $PWD" 
    changes=$( [[ -n $(git status --porcelain) ]] && echo 1 )
    [[ "$changes" ]] && git stash > /dev/null && echo "Stashing changes"
    git pull
    [[ "$changes" ]] && git stash pop > /dev/null && echo "Popping changes"
    cds -
  done
}

# Create a new key, add it to the agent
# $EMAIL, $ALIAS
git-new-key() {
  if [[ -z $1 || -z $2 || -z $3 ]]; then
    echo "Email, Alias, and URL required\n    me@example.com example gitlab.com"
    return
  fi

  ssh-keygen -t rsa -b 4096 -C "$1" -N "" -f ~/.ssh/id_rsa_$2
  eval "$(ssh-agent -s)" >> /dev/null
  echo ""
  ssh-add ~/.ssh/id_rsa_$2

  cat <<EOF >> ~/.ssh/config

Host $2.$3
HostName $3
PreferredAuthentications publickey
IdentityFile ~/.ssh/id_rsa_$2

EOF

  pbcopy < ./id_rsa_$2.pub
  echo "\nPublic key copied to clipboard"
}

# git-aclone alias repo [base dir]
git-aclone() {
  if [[ -z $1 || -z $2 ]]; then
    echo "Repo required\n    git-aclone git@gitlab.com:group/repo.git [alias]\n    git-aclone https://gitlab.com/group/repo.git [email]\n      Optional 3rd parameter for base directory"
    return
  fi

  BASE_DIR=${3:-~}

  if [ -z `echo $1 | sed -n "/^https:/p"` ] ; then
    # SSH
    local -x RIGHT=${1#*git@}  # Right of git@
    local -x  HOST=${RIGHT%:*} # Left of :
    local -x    GR=${RIGHT#*:} # Right of :
    local -x   DIR=$BASE_DIR/$HOST/${GR%/*}
    local -x   END=`basename "$GR"`
    local -x  REPO=${END%.*}
    local -x CLONE=git@$2.$HOST:$GR
    local -x EMAIL=`cat ~/.ssh/id_rsa_$2.pub | sed "s/ssh-rsa .* \(.*\)/\1/"`
  else
    # HTTPS
    local -x RIGHT=${1#*https://}        # Right of https://
    local -x   DIR=$BASE_DIR/${RIGHT%/*} # Left of /*.git
    local -x   END=`basename "$RIGHT"`
    local -x  REPO=${END%.*}
    local -x CLONE=$1
    local -x EMAIL=$2
  fi

  mk $DIR && git clone $CLONE && cd $REPO && git config user.email $EMAIL
}

git-authored() {
  git ls-files | while read f; do git blame --line-porcelain $f | grep '^author '; done | sort -f | uniq -ic | sort -n
}

# GitLab Specific
gitlab-deploy-creds() {
  echo -n "{\"auths\":{\"registry.gitlab.com\":{\"username\":\"$1\",\"password\":\"$2\",\"auth\":\"$(echo -n "$1:$2" | base64)\"}}}" | base64
}

# Open pipeline
git-ci() {
  UI_URL=`git-url`
  open "${UI_URL}/pipelines"
}
alias git-pipeline=git-ci
git-actions() {
  UI_URL=`git-url`
  open "${UI_URL}/actions"
}

git-branch() {
  git checkout -b $1 && git push --set-upstream origin $1
}

git-list() {
  sudo find ${1:-.} -name .git -print0 | xargs -0 -n1 dirname | sort --unique
}

# Remove branches which no longer exist on origin or have been merged
git-prune() {
  BASE_DIR=${1:-.}
  REPOS=$(git-list $BASE_DIR)
  while IFS= read -r dir; do
    echo ----
    echo $dir
    echo Prune Origin
    git --git-dir="${dir}/.git" remote prune origin
    echo Prune Merged
    git --git-dir="${dir}/.git" branch --merged main | grep -v '^[ *]*main$' | xargs git --git-dir="${dir}/.git" branch -d
    echo Prune Branches with no remote
    git --git-dir="${dir}/.git" fetch -p ; git --git-dir="${dir}/.git" branch -r | awk '{print $1}' | egrep -v -f /dev/fd/0 <(git --git-dir="${dir}/.git" branch -vv | grep origin) | awk '{print $1}' | xargs git --git-dir="${dir}/.git" branch -D
    echo ----
  done <<< "$REPOS"
}

git-migrate() {
  BASE_DIR=${3:-.}
  REPOS=$(git-list $BASE_DIR)
  while IFS= read -r dir; do
    echo ----
    echo $dir
    git --git-dir="${dir}/.git" remote set-url origin $(git --git-dir="${dir}/.git" remote get-url --push origin | sed  "s/${1}/${2}/")
    echo ----
  done <<< "$REPOS"
  
}