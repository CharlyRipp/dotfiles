#!/usr/bin/env bash

export GOPATH=$HOME/go
export GOROOT="$(brew --prefix golang)/libexec"
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"

alias go-d='go run *.go'

go-clone() {
    git-clone $1 $GOPATH/src
}