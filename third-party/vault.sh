#!/usr/bin/env bash

# Add the following to your config area
# export VAULT_ADDR=
# export VAULT_ROLE_ID=
# export VAULT_SECRET_ID=
# export VAULT_TOKEN=$(vault-new-token)


vault-new-token() {
    curl --noproxy "*" -s --request POST --data "{\"role_id\":\"${VAULT_ROLE_ID}\",\"secret_id\":\"${VAULT_SECRET_ID}\"}" ${VAULT_ADDR}/v1/auth/approle/login | jq -r '.auth.client_token'
}

vault-update-token() {
    export VAULT_TOKEN=$(vault-new-token)
}