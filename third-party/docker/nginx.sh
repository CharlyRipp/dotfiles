#!/usr/bin/env bash

d-nginx() {
  docker run -d \
    --name nginx-proxy \
    -p 80:80 \
    -v /var/run/docker.sock:/tmp/docker.sock:ro \
    ${DF_DOCKER_MIRROR}/jwilder/nginx-proxy
}

alias d-proxy="docker start nginx-proxy"
