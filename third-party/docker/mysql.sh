#!/usr/bin/env bash

export MYSQL_ROOT_PASSWORD=password

##
# Start a simple MySQL server
#
d-mysql() {
  docker run \
    --name mysql \
    -p 3306:3306 \
    # -v $(pwd)/$2:/docker-entrypoint-initdb.d/init.sql \
    -e MYSQL_DATABASE=db \
    -e MYSQL_USER=user \
    -e MYSQL_PASSWORD=password \
    -e MYSQL_ROOT_PASSWORD=password \
    # -e VIRTUAL_HOST='db.localhost.dstcorp.net' \
    # -e VIRTUAL_PORT=3306 \
    -d ${DF_DOCKER_MIRROR}/mysql:latest
}

d-mysql-connect() {
  docker exec -it mysql bash -l -c 'mysql -uroot -p$MYSQL_ROOT_PASSWORD'
}
