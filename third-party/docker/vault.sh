#!/bin/bash

d-vault-create() {
    if [ ! -z "$VAULT_ADDR" ]; then 
        echo "Backing up VAULT_ADDR to \$VAULT_ADDR_BKUP"
        export $VAULT_ADDR_BKUP=$VAULT_ADDR
    fi
    if [ ! -z "$VAULT_TOKEN" ]; then 
        echo "Backing up VAULT_TOKEN to \$VAULT_TOKEN_BKUP"
        export $VAULT_TOKEN_BKUP=$VAULT_TOKEN
    fi
    
    export VAULT_ADDR=http://127.0.0.1:8200
    docker run -p 8200:8200 --add-host="host:`whats-my-internal-ip`" -d --name=vault --cap-add=IPC_LOCK -e 'VAULT_LOCAL_CONFIG={"backend": {"etcd": {"address": "http://host:2379", "etcd_api": "v3"}}, "default_lease_ttl": "168h", "max_lease_ttl": "720h", "listener":{"tcp":{"address":"0.0.0.0:8200", "tls_disable":1}}}' ${DF_DOCKER_MIRROR}/vault server

    # Init and save Unseal Key
    sleep 1
    echo "Vault started - Initializing..."
    VAULT_INFO=$((vault init -key-shares=1 -key-threshold=1) 2>&1)

    # Save Vault Tokens
    export VAULT_UNSEAL_KEY=$((echo "$VAULT_INFO"| grep '^Unseal' | awk '{print $4}') 2>&1)

    export VAULT_TOKEN=$(echo "$VAULT_INFO" | grep '^Initial' | awk '{print $4}')

    # Unseal Vault
    echo "Unsealing..."
    vault unseal $VAULT_UNSEAL_KEY

    # Authenticate
    echo "Authenticating..."
    vault auth $VAULT_TOKEN
}

d-vault-start() {
    docker start etcd >> /dev/null
    if health-check http://127.0.0.1:2379/health 1 10; then
        docker start vault >> /dev/null
        echo "Vault started"

        if [ "$VAULT_TOKEN" != "$VAULT_TOKEN" ]; then 
            echo "Backing up VAULT_TOKEN to \$VAULT_TOKEN_BKUP"
            export $VAULT_TOKEN_BKUP=$VAULT_TOKEN

        fi
        export VAULT_TOKEN=$VAULT_TOKEN_LOCAL
    fi
}