#!/usr/bin/env bash

d-kafka-compose() {
  PORT=${1:-9092}

  cat <<EOF
version: '2'

networks:
  kafka-net:
    driver: bridge

services:
  zookeeper:
    container_name: kafka-zookeeper
    image: ${DF_DOCKER_MIRROR}/bitnami/zookeeper:latest
    networks:
      - kafka-net
    ports:
      - '2181:2181'
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes
  kafka:
    container_name: kafka
    image: ${DF_DOCKER_MIRROR}/bitnami/kafka:latest
    networks:
      - kafka-net    
    ports:
      - '${PORT}:9092'
      - '29094:29094'
    environment:
      KAFKA_LISTENERS: LISTENER_I://kafka:29092,LISTENER_E://localhost:9092
      KAFKA_ADVERTISED_LISTENERS: LISTENER_I://kafka:29092,LISTENER_E://localhost:9092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: LISTENER_I:PLAINTEXT,LISTENER_E:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: LISTENER_I
      KAFKA_CFG_ZOOKEEPER_CONNECT: zookeeper:2181
      ALLOW_PLAINTEXT_LISTENER: "yes"
    depends_on:
      - zookeeper
EOF
}

d-kafka-up() {
  d-kafka-compose $1 | docker-compose -f - up -d
}
d-kafka-down() {
  d-kafka-compose $1 | docker-compose -f - down
}

d-kafka-list-topics() {
  docker exec -t kafka kafka-topics.sh --bootstrap-server :9092 --list
}

d-kafka-create-topic() {
  TOPIC=${1:-topic}
  PARTITIONS=${2:-1}
  REPLICAS=${3:-1}
  docker exec -t kafka kafka-topics.sh --bootstrap-server :9092 --create --replication-factor $REPLICAS --partitions $PARTITIONS --topic $TOPIC
}

d-kafka-delete-topic() {
  TOPIC=${1:-topic}
  docker exec -t kafka kafka-topics.sh --bootstrap-server :9092 --delete --topic $TOPIC
}

d-kafka-topic() {
  TOPIC=${1:-topic}
  CMD=${2:---describe}
  docker exec -t kafka kafka-topics.sh --bootstrap-server :9092 --topic ${TOPIC} ${CMD}
}

d-kafka-topic-count() {
  TOPIC=${1:-topic}
  CONSUMER=${2:-consumer}
  docker exec -t kafka kafka-consumer-offset-checker.sh --bootstrap-server :9092 --topic ${TOPIC} $3 --group=${CONSUMER}
}

d-kafka-group() {
  TOPIC=${1:-topic}
  CONSUMER=${2:-consumer}
  docker exec -t kafka kafka-consumer-groups.sh --bootstrap-server :9092 --group ${CONSUMER} --describe
}

d-kafka-listen() {
  TOPIC=${1:-topic}
  docker exec -t kafka kafka-console-consumer.sh --topic ${TOPIC} --from-beginning --bootstrap-server :9092
}

d-kafka-group-reset() {
  TOPIC=${1:-topic}
  CONSUMER=${2:-consumer}
  docker exec -t kafka kafka-consumer-groups.sh --bootstrap-server :9092 --group ${CONSUMER} --topic ${TOPIC} --reset-offsets --to-earliest --execute
}

