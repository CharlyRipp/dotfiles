#!/usr/bin/env bash

##
# Start a keycloak server
#
# d-keycloak() {
#   docker run \
#   --name keycloak \
#   -p 9999:8080 \
#   -e KEYCLOAK_USER=admin \
#   -e KEYCLOAK_PASSWORD=password \
#   -d jboss/keycloak:6.0.1
# }

d-keycloak-compose() {
  cat <<EOF
version: '3'

volumes:
  postgres_data:
      driver: local

services:
  postgres:
      container_name: keycloak-db
      image: ${DF_DOCKER_MIRROR}/postgres
      volumes:
        - postgres_data:/var/lib/postgresql/data
      environment:
        POSTGRES_DB: keycloak
        POSTGRES_USER: keycloak
        POSTGRES_PASSWORD: password
  keycloak:
      container_name: keycloak
      image: ${DF_DOCKER_MIRROR}/jboss/keycloak:9.0.3
      environment:
        DB_VENDOR: POSTGRES
        DB_ADDR: postgres
        DB_DATABASE: keycloak
        DB_USER: keycloak
        DB_SCHEMA: public
        DB_PASSWORD: password
        KEYCLOAK_USER: admin
        KEYCLOAK_PASSWORD: password
        JAVA_TOOL_OPTIONS: -Dkeycloak.profile.feature.token_exchange=enabled
        # Uncomment the line below if you want to specify JDBC parameters. The parameter below is just an example, and it shouldn't be used in production without knowledge. It is highly recommended that you read the PostgreSQL JDBC driver documentation in order to use it.
        #JDBC_PARAMS: "ssl=true"
      ports:
        - 9999:8080
      depends_on:
        - postgres
EOF
}

d-keycloak-up() {
  d-keycloak-compose | docker-compose -f /dev/stdin up -d
}
d-keycloak-down() {
  d-keycloak-compose | docker-compose -f /dev/stdin down
}
