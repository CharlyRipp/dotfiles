export PATH="/usr/local/opt/python/libexec/bin:$PATH"

alias pip-clear='pip freeze | xargs pip uninstall -y'
alias pip-init='pip install -r requirements.txt'