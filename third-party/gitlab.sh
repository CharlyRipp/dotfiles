#!/bin/bash

gitlab-runner-start() {
  docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v ~/gitlab.com/runner/config.toml:/etc/gitlab-runner/config.toml \
    gitlab/gitlab-runner:latest
}

gitlab-runner-stop() {
  docker stop gitlab-runner
}
