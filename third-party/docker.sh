#!/usr/bin/env bash

# Start docker (mac)
d-start() {
  open --background -a Docker \
    && while ! docker system info > /dev/null 2>&1; do sleep 1; done
}

# Exit docker (mac)
d-kill() {
  test -z "$(docker ps -q 2>/dev/null)" && osascript -e 'quit app "Docker"'
}

# Clean up stopped containers
alias d-prune='docker system prune -af && docker system prune -af --volumes'

# Kill and clean up all containers
alias d-stop='docker stop $(docker ps -aq)'

# Remove all stopped containers
alias d-rm='docker rm $(docker ps -aq)'

# Remove all stopped containers
alias d-wipe='d-stop && d-rm'

# Short-hand list all containers
alias d-ps='docker ps -a'

# Play with a container
d-play() {
  docker run -it --rm --name ${3:-playground} $1 ${2:-/bin/bash}
}

# Pull container appending mirror, then retagging to use without 
d-pull() {
  docker pull ${DF_DOCKER_MIRROR}/$1
  docker tag ${DF_DOCKER_MIRROR}/$1 $1
  docker rmi ${DF_DOCKER_MIRROR}/$1
}

#####################################
# --- Latest Container commands --- #

# Remote into the latest container
alias d-rsh='docker exec -it $(docker ps -lq) /bin/bash'

# Follow logs of latest container
alias d-logs='docker logs -f $(docker ps -lq)'

# Top in latest container
alias d-top='docker top $(docker ps -lq)'

source-all $DF_DIR/third-party/docker
