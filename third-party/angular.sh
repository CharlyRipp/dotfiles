#!/usr/bin/env bash

alias ng-n='ng new --style=scss --strict --routing'

ngu-new() {
  ng new $1 -p demo --style=scss --strict --routing
  cd $1
  ng add @nguniversal/express-engine
  ng g m home --route rootroute -m app
  sed -i '' "s+rootroute++g" "src/app/app-routing.module.ts"
}

ng-lib() {
  ng g lib -p ripp $1
  sed -i '' "s+$1+@ripp/$1+g" "projects/$1/package.json"
  ng g m demo-$1 --route $1 -m ${2:-app}
  mv src/app/demo-$1 src/app/$1
}